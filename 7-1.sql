/*
Определить продавцов, которые обслуживают регион 'Northern' (таблица region).
*/

SELECT 
	DISTINCT employees.employee_id, employees.first_name, employees.last_name, region.region_description

FROM
	employees
    INNER JOIN employee_territories ON employees.employee_id = employee_territories.employee_id
    INNER JOIN territories ON employee_territories.territory_id = territories.territory_id
    INNER JOIN region ON territories.region_id = region.region_id

WHERE 
    region.region_description = 'Northern'

/*
UPD: почитал немного подробнее про виды JOIN-ов - ошибку понял. Изменил LEFT JOIN на INNER JOIN (эквивалентно JOIN)
*/