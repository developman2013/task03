/*
Выбрать из таблицы supplier всех поставщиков, расположенных в UK и France (колонка country). 
Запрос сделать с только помощью оператора IN. 
Возвращать company_name, contact_name, city, phone. Упорядочить результаты запроса по country и company_name
*/
SELECT company_name, contact_name, city, phone 

FROM suppliers

WHERE country IN ('UK', 'France')

ORDER BY country, company_name

/*
Таблица в БД прописана во множественном числе: suppliers
*/