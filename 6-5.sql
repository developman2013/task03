/*
По таблице employees найти для каждого продавца его руководителя.
*/

SELECT
	employees.first_name, employees.last_name , suppliers.contact_name AS "Supervisor"

FROM
	suppliers, employees

WHERE
	employees.reports_to = suppliers.supplier_id
	
ORDER BY 
	employees.first_name

--MSG: таблицы с руководителями нет в базе, 
--	   для примера осуществлена подвязка к полю <reports_to>