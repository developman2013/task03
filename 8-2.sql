/*
Выдать всех заказчиков (таблица customers), которые не имеют 
ни одного заказа (подзапрос по таблице orders). 
Использовать оператор EXISTS. 
В результате вывести company_name, contact_name, country.
*/

SELECT
	customers.company_name, customers.contact_name, customers.country

FROM
	customers

WHERE
	NOT EXISTS 
		(
			SELECT 
				customer_id
			FROM
				orders
			WHERE
				orders.customer_id = customers.customer_id
        )

/*
UPD: Я понял свою ошибку, исправил. Аналогично можно переписать запрос без EXISTS:

SELECT
	customers.company_name, customers.contact_name, customers.country

FROM
	customers

WHERE
	customers.customer_id NOT IN 
		(
			SELECT 
				customer_id
			FROM
				orders
        )
*/