/*
Выбрать всех заказы из таблицы orders, у которых заказчик (customer_id) начинается на буквы из диапазона C и N. 
Использовать оператор BETWEEN. Запрос должен возвращать только колонки order_id и customer_id , ship_country и отсортирован по ship_country.
*/

SELECT order_id, customer_id, ship_country

FROM orders 

WHERE LEFT(customer_id,1) BETWEEN 'C' AND 'N'

ORDER BY ship_country