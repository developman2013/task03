/*
Выдать всех продавцов, которые имеют более 20 заказов. 
Использовать вложенный SELECT. 
В результате вывести first_name, last_name, region, country.
*/

SELECT 
	first_name, last_name, region, country
FROM
	employees
WHERE 
	employee_id IN (SELECT 
						employees.employee_id
					FROM 
						orders, employees
					GROUP BY 
						employees.employee_id
				   	HAVING
						COUNT(orders.order_id) > 20
				   )

/*
>По запросу 8.1 - перепишите его так, 
что бы в подзапросе ыбли использованы колонки 
из основного запроса, сейчас у вас поздапрос самостоятелен,  
нужно сделать чот бы он был зависим от данных основного запроса. 

 UPD: Изменил поле orders.employee_id на employees.employee_id. 
 Других способов установить ещё более жесткую связь запроса и подзапроса я пока не нашёл.
 Можно, конечно, в подзапросе строить связь двух таблиц и возвращать first/lastname, 
 но это только породит "костыль", из-за которого при наличии двух продавцов 
 с одинаковыми first/lastname будет считать за одного человека.
*/