/*
По таблице orders найти количество заказов, которые еще не были доставлены 
(т.е. в колонке shipped_date нет значения даты доставки). 
Использовать при этом запросе только оператор COUNT. 
Не использовать предложения WHERE и GROUP.
*/

SELECT (COUNT(order_id)-COUNT(shipped_date)) AS "Totals"

FROM orders