/*
Написать запрос, который выводит заказы без указанного ship_region из таблицы orders. В результатах запроса возвращать для колонки ship_region вместо значений NULL строку ‘Not set’ (использовать системную функцию CASЕ). Запрос должен возвращать только колонки order_id, order_date, ship_country, ship_region и ship_city
*/

SELECT order_id, order_date, ship_country,
  CASE
    WHEN ship_region IS NULL THEN 'Not set'
  END ship_region,
  ship_city

FROM orders

WHERE ship_region IS NULL