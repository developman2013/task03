/*
Выбрать в таблице orders заказы, которые были сделаны после 16 августа 2017 года (колонка order_date) включительно и которые доставлены с ship_via =3. Запрос должен возвращать только колонки order_id, customer_id, order_date, и ship_via.
*/

SELECT order_id, customer_id, order_date, ship_via 

FROM orders

WHERE order_date > '2017-08-16' AND ship_via = 3