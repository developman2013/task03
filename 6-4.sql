/*
Найти всех покупателей, которые живут в одном городе. 
Показывать те города, в которых живет более 2 покупателя в одном городе.
*/

SELECT 
    COUNT(customer_id), city 
FROM 
    customers 
GROUP BY 
    city 
HAVING 
    COUNT(customer_id) >=3