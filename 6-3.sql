/*
По таблице orders найти количество заказов, выполненных каждым продавцом (employee_id) и для каждого покупателя(customer_id). 
Необходимо определить это только для заказов, сделанных в 2017 году.
*/

SELECT 
    (employees.first_name || ' ' || employees.last_name) AS "Продавец", 
    customers.contact_name AS "Покупатель",
    COUNT(orders.order_id) AS "Заказов"

FROM orders, employees, customers

WHERE 
    employees.employee_id = orders.employee_id AND
    orders.customer_id = customers.customer_id AND
    EXTRACT(YEAR FROM order_date) = '2017'

GROUP BY(employees.employee_id, customers.customer_id)

ORDER BY ("Продавец")