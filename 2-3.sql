/*
Выбрать всех заказы из таблицы orders, у которых название страны доставки (ship_country) 
начинается на буквы из диапазона b и g, не используя оператор BETWEEN.
*/

SELECT *

FROM orders 

WHERE LEFT(ship_country,1) >= 'b' AND LEFT(ship_country,1) < 'h'

ORDER BY ship_country