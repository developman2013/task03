/*
Выдать в результатах запроса имена всех заказчиков из таблицы customers 
и суммарное количество их заказов из таблицы orders. 
Принять во внимание, что у некоторых заказчиков нет заказов, 
но они также должны быть выведены в результатах запроса. 
Упорядочить результаты запроса по возрастанию количества заказов.
*/

SELECT 
	contact_name AS "Customer name", COUNT(orders.order_id) AS "Total orders"

FROM
	customers
	LEFT JOIN orders ON customers.customer_id = orders.customer_id

GROUP BY 
    contact_name
    
ORDER BY 
    "Total orders"