/*
Выбрать все поставщиков (supplier_id) из таблицы products (поставщики в результате не должны повторяться), 
где цена за единицу равна *10-19 *включительно – это колонка unit_price в таблице products. 
Использовать оператор BETWEEN. Запрос должен возвращать только колонку supplier_id.
*/

SELECT DISTINCT supplier_id 

FROM products

WHERE unit_price BETWEEN 10 AND 19

ORDER BY supplier_id