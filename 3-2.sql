/*
Выбрать из таблицы supplier всех поставщиков, не расположенных в UK и France (колонка country). 
Запрос сделать с только помощью оператора IN. Возвращать company_name, contact_name, city, phone. 
Упорядочить результаты запроса по country по возрастанию и company_name по убыванию.
*/

SELECT company_name, contact_name, city, phone

FROM suppliers

WHERE country NOT IN ('UK', 'France')

ORDER BY country ASC, company_name DESC

/*
Таблица в БД прописана во множественном числе: suppliers
*/