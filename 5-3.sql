/*
По таблице orders найти количество различных покупателей (customer_id), сделавших заказы. 
Использовать функцию COUNT и не использовать предложения WHERE и GROUP.
*/

SELECT COUNT(DISTINCT customer_id)

FROM orders