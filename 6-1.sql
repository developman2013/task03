/*
По таблице orders найти количество заказов с группировкой по годам. 
В результатах запроса надо возвращать две колонки c названиями Год и Количество.
*/

SELECT EXTRACT(YEAR FROM order_date) AS "Год", COUNT(order_id) AS "Количество"

FROM orders

GROUP BY(EXTRACT(YEAR FROM order_date))

ORDER BY(EXTRACT(YEAR FROM order_date))