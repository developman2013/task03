/*
По таблице orders найти количество заказов, обслуженным каждым продавцом. 
Колонка employee_id является идентификатором. 
В результатах выполнения запроса нужно показать имя и фамилию продавца в одной колонке 
(объединение last_name и first_name). 
Для получения Имя/фамилия продавца НЕ использовать подзапрос. 
Основная группировка по employee_id . 
В результате вывести две колонки: “Продавец” (объединение last_name и first_name) 
и колонку c количеством заказов возвращать с названием “Заказов”. 
Результаты запроса должны быть упорядочены по убыванию количества заказов.
*/

SELECT 
    (employees.first_name || ' ' || employees.last_name) AS "Продавец", 
    COUNT(orders.order_id) AS "Заказов"

FROM orders, employees

WHERE employees.employee_id = orders.employee_id

GROUP BY(employees.employee_id)

ORDER BY ("Заказов") DESC